# Testcicd

test for ci cd

## install docker  
```
curl https://get.docker.com | sudo sh
systemctl start docker
systemctl enable docker
```
## добавим себя в группу docker:
```
sudo usermod -aG docker ${USER}
$ su - ${USER}
```

## install docker-compose  
```
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## no install minikube  
```
https://minikube.sigs.k8s.io/docs/start/
```

## mqttwarn  
```
https://github.com/jpmens/mqttwarn
```
